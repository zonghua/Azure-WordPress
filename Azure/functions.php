<?php
function ms_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>" class="comment-body">
<div class="comment-author">
<?php echo get_avatar( $comment, $size = '32'); ?>
<cite class="fn"><?php printf(__('%s'), get_comment_author_link()) ?></cite>
</div>
<div class="comment-meta">
<?php printf(__('%s'), get_comment_date("Y/m/d") ) ?>
</div>
<div class="comment-content">
<p><?php comment_text() ?></p>
</div>
<div class="comment-reply">
<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __('回复')))) ?>
</div>
</div>
<?php
}	
?>
<?php
//检测主题更新
include_once("theme-updates/theme-update-checker.php");
$MyThemeUpdateChecker = new ThemeUpdateChecker(
'Azure', //Theme slug. Usually the same as the name of its directory.
'http://www.mywpku.com/wp-update-server/?action=get_metadata&slug=Azure' //Metadata URL.
);
//短代码
//警示
function warningbox($atts, $content=null, $code="") {
	$return = '<div class="alert warning">';
	$return .= $content;
	$return .= '</div>';
	return $return;
}
add_shortcode('warning' , 'warningbox' );
//危险
function dangerbox($atts, $content=null, $code="") {
	$return = '<div class="alert danger">';
	$return .= $content;
	$return .= '</div>';
	return $return;
}
add_shortcode('danger' , 'dangerbox' );
//成功
function successbox($atts, $content=null, $code="") {
	$return = '<div class="alert success">';
	$return .= $content;
	$return .= '</div>';
	return $return;
}
add_shortcode('success' , 'successbox' );
//信息
function infobox($atts, $content=null, $code="") {
	$return = '<div class="alert info">';
	$return .= $content;
	$return .= '</div>';
	return $return;
}
add_shortcode('info' , 'infobox' );
//检测主题更新
require_once(TEMPLATEPATH . '/theme-updates/theme-update-checker.php'); 
$wpdaxue_update_checker = new ThemeUpdateChecker(
	'theme_aaa', //主题名字
	'http://img.mywpku.com/Azure-info.json'  //info.json 的访问地址
);
add_theme_support( 'post-formats', array('status') );
register_nav_menus(
array(
'sidebar-nav' => __( '左侧导航' ),
)
);
function pagination($query_string){
global $posts_per_page, $paged;
$my_query = new WP_Query($query_string ."&posts_per_page=-1");
$total_posts = $my_query->post_count;
if(empty($paged))$paged = 1;
$prev = $paged - 1;
$next = $paged + 1;
$range = 2; // only edit this if you want to show more page-links
$showitems = ($range * 2)+1;
 
$pages = ceil($total_posts/$posts_per_page);
if(1 != $pages){
echo "<div class='page-nav'>";
echo "<div class='page-navigator'>";
echo ($paged > 1 && $showitems < $pages)? "
<a href='".get_pagenum_link($prev)."'>上一页</a>":"";
 
for ($i=1; $i <= $pages; $i++){
if (1 != $pages &&( !($i >= $paged+$range+1 ||
    $i <= $paged-$range-1) || $pages <= $showitems )){
echo ($paged == $i)? "<span class='current'>".$i."</span>":
"<a href='".get_pagenum_link($i)."' >".$i."</a>";
}
}
 
echo ($paged < $pages && $showitems < $pages) ?
"<a href='".get_pagenum_link($next)."' class='next'>下一页</a>" :"";
echo "</div>";
echo "</div>\n";
}
}
/*
 * 替代 mb_strimwidth
 */
function my_mb_strimwidth($str ,$start , $width , $encoding ,$trimmarker ){
    $output = preg_replace('/^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start.'}((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$width.'}).*/s','\1',$str);
    return $output.$trimmarker;
}